import React from 'react'
import styleClasses from './Burger.css';
import BurgerIngredients from './BurgerIngredients/BurgerIngredients';

/* jshint expr: true */
const burger = (props) => { 

    let transformedIngredients = Object.keys(props.ingredients).map(igKey =>{
        return [...Array(props.ingredients[igKey])].map((_,i)=>{
            return <BurgerIngredients key={igKey + 1} type={igKey} />;
        });
    }).reduce((arr,el)=>{
        return arr.concat(el); 
    },[]);

    if(transformedIngredients.length === 0){
        transformedIngredients = <p>Please start adding Ingredients!</p>;
    }
    console.log(transformedIngredients);

    return(
        <div className={styleClasses.Burger}>
            <BurgerIngredients type="bread-top" />
            {transformedIngredients}
            <BurgerIngredients type="bread-bottom" />
        </div>
    );
};

export default burger;