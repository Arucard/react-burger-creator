import React from 'react';
import Aux from '../../hoc/Auxilary';
import cssClasses from './Layout.css'

const layout = (props) => ( //Error cus of Adjacent jsx elements
    // OPtion 1 return array and each element has a key
// Option 2 high order/level utility/auxilary component
// Option 3 Wrap everything in a div
<Aux>
    <div>Toolbar, SideDrawer, Backdrop</div>
    <main className={cssClasses.TopMargin}>
        {props.children}
    </main>
</Aux>
);

export default layout;
